#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <errno.h>

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

typedef struct _codeflag
{
  char line[512];
  char code[1024];
  int lineno;
  int type;
  struct _codeflag *next;
} codeflag;

codeflag *entries[0];

char *functions[] =
  { "wpdb", "add_action", "wp_verify_nonce", "function_exists", "is_admin",
  "ABSPATH", "current_user_can"
};

char *input_functions[] =
  { "$_FILE", "$_POST", "$_GET", "$_post", "$_get", "$_REQUEST", "SELECT",
  "Select", "INSERT", "Insert"
};
char *includes[] = { "require_once" };

void init_struct (codeflag * cf[]);
void display (codeflag * cf[], int x, int ansi);
void insert_node (codeflag * cf[], char *s, int lineno, char *i, int t);
int
main (int argc, char **argv)
{

  FILE *fin, *art, *flist;
  char buffer[256];
  char tempb[256];
  char filename[256];
  char next_file[256];
  int input_total = 0,include_total=0;
  int c, error_fl;
  int wp_api_total = 0, ans = 0;
  int x, line = 1, interest = 0, found = 0;

  if (argc == 1)
    {
      printf ("Please use -h for help.\n");
      exit (0);
    }

  while ((c = getopt (argc, argv, "hal:")) != -1)
    switch (c)
      {
      case 'a':
	ans = 1;
	break;
      case 'h':
	printf ("%s -l <filelist> -a enable ANSI output.\n", argv[0]);
	exit (0);
      case 'l':
	strcpy (filename, optarg);
	break;
      case '?':
	if (optopt == 'l')
	  fprintf (stderr, "Option -%c requires an argument.\n", optopt);
	else if (isprint (optopt))
	  fprintf (stderr, "Unknown option `-%c'.\n", optopt);
	else
	  fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
	return 1;
      default:
	abort ();
      }

  flist = fopen (filename, "r");
  if (flist <= 0)
    {
      printf ("\nError %d: File %s not found.\n", (int) flist, argv[1]);
      exit (0);
    }

  while (fgets (next_file, sizeof (next_file), flist))
    {
      init_struct (entries);
      next_file[strlen (next_file) - 1] = '\0';
      errno = 0;
      fin = fopen (next_file, "r");
      error_fl = errno;

      if (fin <= 0)
	{
	  printf ("\nError %d: File [%s] not found %s.\n", (int) fin,
		  next_file, (char *) strerror (error_fl));
	  exit (0);
	}
      while (fgets (buffer, sizeof (buffer), fin) != NULL)
	{
	  if (buffer[0] != '#')
	    {
	      buffer[strlen (buffer) - 1] = '\0';	/*strip off newline */
	      for (x = 0; x <= 6; x++)
		{
		  if (strstr (buffer, functions[x]))
		    {
		      sprintf (tempb,
			       "Found WordPress API Function [%s] \n-> %d: %s",
			       functions[x], line, buffer);
		      insert_node (entries, buffer, line, tempb, 1);
		      found++;
		    }
		}
	      for (x = 0; x < 1; x++)
		{
		  if (strstr (buffer, includes[x]))
		    {
		      sprintf (tempb,
			       "Found Include Function [%s] \n-> %d: %s",
			       includes[x], line, buffer);
		      insert_node (entries, buffer, line, tempb, 0);
		      include_total++;
		    }
                 }
	      for (x = 0; x <= 9; x++)
		{
		  if (strstr (buffer, input_functions[x]))
		    {
		      sprintf (tempb,
			       "Found User Input Function [%s] \n-> %d: %s",
			       input_functions[x], line, buffer);
		      insert_node (entries, buffer, line, tempb, 2);
		      interest++;
		    }
		}
	    }
/*Enter data for this line after it has been processed*/
	  line++;
	} /*read line from php file*/
	  if (found > 0 || interest > 0)
	    {
	      input_total += interest;
	      wp_api_total += found;
	      interest = 0;
	      found = 0;
            }
      if (input_total > 0)
	{
      if (ans)
	{
	  printf ("%s", KBLU);
	  printf
	    ("##################################################################################\n");
	  printf
	    ("#                        WordPress Plugin Vulnerability Ferret                   #\n");
	  printf
	    ("#                                         v1.9                                   #\n");
	  printf
	    ("##################################################################################\n");
	  printf ("%s", KNRM);
	  art = fopen ("./art.txt", "r");
	  if (art <= 0)
	    {
	      printf ("\nartwork file not found.\n");
	    }
	  else
	    {
	      while (fgets (buffer, sizeof (buffer), art))
		{
		  printf ("%s", buffer);

		}
	    }
	}
	  printf ("Checking %s:\n", next_file);
	  display (entries, line, ans);
	  printf ("Found %d User Inputs and %d calls to an WP API function with %d includes\n",
		  input_total, wp_api_total,include_total);
	  printf
	    ("[=================================================================================]\n");
	}
	  free (entries[0]);
          found=0;
          interest=0;
	  input_total = 0;
	  line = 0;
	  wp_api_total = 0;
	      include_total = 0;
      fclose (fin);
    }
  fclose (flist);
  return (0);
}




void
init_struct (codeflag * cf[])
{
  /*
   */
  cf[0] = (codeflag *) malloc (sizeof (codeflag));
  cf[0]->line[0] = '\0';
  cf[0]->code[0] = '\0';
  cf[0]->next = NULL;
}


void
insert_node (codeflag * cf[], char *s, int lineno, char *i, int t)
{
  /*
   * ## This procedure adds a node into anywhere on the list.
   * ## it returns nothing. 
   */
  codeflag *ptr, *new;
  ptr = cf[0];			/* Line number is hash value. */
  new = (codeflag *) malloc (sizeof (codeflag));
  new->lineno = lineno;
  new->type = t;
  strcpy (new->line, s);
  strcpy (new->code, i);
  new->next = NULL;
  while (ptr->next != NULL)
    ptr = ptr->next;
  ptr->next = new;
}


void
display (codeflag * cf[], int x, int ansi)
{

/*## Just displays the link list. returns nothing.
   ## it scrolls through the table and down each list. */
  codeflag *ptr, *tmp;
  ptr = cf[0]->next;		/* Line number is hash value. */
  while (ptr)
    {
      if (ansi)
	{
	  if (ptr->type == 1)
	    printf ("%s %s %s\n", KYEL, ptr->code, KNRM);
	  if (ptr->type == 2)
	    printf ("%s %s %s\n", KRED, ptr->code, KNRM);
	  if (ptr->type == 0)
	    printf ("%s %s %s\n", KMAG, ptr->code, KNRM);
	}
      else
	{
	    printf ("%s\n", ptr->code);

	}
      tmp = ptr;
      ptr = ptr->next;
      free (tmp);
    }
}